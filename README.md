There are two files "Particles.h" and "UI.cpp" containing the class design an the command line interface for users repectively ParticleBase.

The commands for installing and running are as follows. The compilation option uses C++ 11.

    $ g++ -std=c++11 UI.cpp -o Particle.o
    $ Particle.o
   
The user commands will be displayed and the user can get relevant information through this.

Alternatively, the GNU Make utility and Makefile could be used in UNIX environments.