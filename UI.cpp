#include <bits/stdc++.h>
#include <fstream>
#include <iostream>
#include "Particles.h"

using namespace std;

int ParticleBase::countParticle = 0;
int Lepton::countLepton = 0;
int Photon::countPhoton = 0;
int Jet::countJet = 0;

int main(){

    /* Since there are three types of particles, let us consider particles get repeated in cycles of 3 */
    ifstream fin("input.dat");
    vector<Lepton> leptons;
    vector<Photon> photons;
    vector<Jet> jets;
    
    int state = 0;
    double px, py, pz, e;
    while (fin >> px >> py >> pz >> e){
           if(state == 0){
               Lepton lepton(px,py,pz,e);
               // lepton.printValues();
               leptons.push_back(lepton);
               state = (state+1)%3;
           }   
           else if(state == 1){
               Photon photon(px,py,pz,e);
               // photon.printValues();
               photons.push_back(photon);
               state = (state+1)%3;
           }
           else{
               Jet jet(px,py,pz,e);
               // jet.printValues();
               jets.push_back(jet);
               state = (state+1)%3;
           }     
    }

    /* User Commands handling starts, we define some basic queries which user can run */
    cout << "Use thefollowing codes\n1: Paricle Count\n2: Particle Properties\n3: Exit\n";
    int command;
    while(cin>>command){
        if(command == 0){
           cout << "Terminated" << endl;
           break;
        }
        else if(command == 1){
            cout << "Total number of particles: " << ParticleBase::getCount() << endl;
            cout << "Total number of leptons: " << Lepton::getLeptonCount() << endl;
            cout << "Total number of photons: " << Photon::getPhotonCount() << endl;
            cout << "Total number of jets: " << Jet::getJetCount() << endl;
            /* Alternatively, we can also print the sizes of vectors here */
        }
        else if(command == 2){
            cout << "Enter the particle type and index:\n1: Lepton\n2: Photon\n3: Jet\n";
            int type, index;
            cin>>type>>index;
            if(type == 1){
                leptons[index].printValues();
            }
            else if(type == 2){
                photons[index].printValues();
            }
            else if(type == 3){
                jets[index].printValues();
            }
        }
        else{
            cout << "Invalid command" << endl;
        }   
    }
    return 0;
}