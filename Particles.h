using namespace std;

class ParticleBase{
    /* All base classes share the same static variable, so we will use this to store the count */
    protected:
        static int countParticle;
        double px, py, pz, e;
    
    /* I have kept the functions public so that we can access the protected variables even outside of derived classes*/
    public:
        ParticleBase(double px, double py, double pz, double e){
            this->px = px;
            this->py = py;
            this->pz = pz;
            this->e = e;
            countParticle++;
        }
        static int getCount(){
            return countParticle;
        }
        void printValues(){
            cout << "px: " << px << " py: " << py << " pz: " << pz << " e: " << e << endl;
        }
        /** Utility Functions, more such can be added */
        double computeP2(){
            return px*px + py*py;
        }
        double computeP3(){
            return px*px + py*py + pz*pz;
        }    
};


class Lepton : public ParticleBase
{
    protected:
        static int countLepton; 

    public:
        Lepton(double px, double py, double pz, double e): ParticleBase(px, py, pz, e){
            countLepton++;
        }

        static int getLeptonCount(){
            return countLepton;
        }
        /* Instead of having 2 such functions, we can have a single boolean variable storing state */
        bool isMuon(){
            return (e > 0);
        }
        bool isElectron(){
            return (e < 0);
        }
};

class Photon : public ParticleBase
{
    protected:
        static int countPhoton; 

    public:
        Photon(double px, double py, double pz, double e): ParticleBase(px, py, pz, e){
            countPhoton++;
        }
        static int getPhotonCount(){
            return countPhoton;
        }
};

class Jet : public ParticleBase
{
    protected:
        static int countJet; 

    public:
        Jet(double px, double py, double pz, double e): ParticleBase(px, py, pz, e){
            countJet++;
        }
        static int getJetCount(){
            return countJet;
        }
};